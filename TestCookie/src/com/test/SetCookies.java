package com.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SetCookies extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		for (int i = 0; i < 3; i++) {
			Cookie cookie = new Cookie("Session-Cookie-" + i, "Cookie-Values-S"
					+ i);
			response.addCookie(cookie);
			cookie = new Cookie("Persistent-Cookie-" + i, "Cookie-value-P" + i);
			cookie.setMaxAge(3600);
			response.addCookie(cookie);
		}

		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>设置Cookie</TITLE></HEAD>");
		out.println("  <BODY>");
		out.println("<h1 align='center'>设置Cookie<h1>");
		out.println("6个Cookie");
		out.println("<a href='ShowCookies'>\n" + "查看</a>");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

}
