create database mobileshop;
use mobileshop;
create table user(
logname varchar(255) NOT NULL PRIMARY KEY,
password varchar(255),
phone varchar(255),
address varchar(255),
realname varchar(255)
);

create table mobileClassify(
id int(11) not null auto_increment,
name char(200) not null,
primary key (id)
);



create table mobileForm(
mobile_version varchar(50) not null ,
mobile_name varchar(60),
mobile_made varchar(200),
mobile_price float(4),
mobile_mess varchar(255),
mobile_pic varchar(200) not null, 
id int(11) not null,
primary key(mobile_version,mobile_pic),
FOREIGN KEY(id) REFERENCES mobileClassify(id)
);

create table orderForm(
id int(10) not null auto_increment,
logname varchar(255),
mess varchar(255),
sum float,
primary key (id)
);


