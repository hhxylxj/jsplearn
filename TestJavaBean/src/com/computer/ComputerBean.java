package com.computer;

public class ComputerBean {
	double numberOne, numberTwo, result;
	String operator = "+";
	public double getNumberOne() {
		return numberOne;
	}
	public void setNumberOne(double n) {
		numberOne = n;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String s) {
		operator = s.trim();
	}
	public double getNumberTwo() {
		return numberTwo;
	}
	public void setNumberTwo(double n) {
		numberTwo = n;
	}
	public double getResult() {
		if (operator.equals("+"))
			result = numberOne + numberTwo;
		else if (operator.equals("-")) {
			result = numberOne - numberTwo;
		} else if (operator.equals("*")) {
			result = numberOne * numberTwo;
		} else if (operator.equals("/")) {
			result = numberOne / numberTwo;
		}
		return result;
	}
}
