package com.circle;

public class Circle {
	int radius;

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	public double circleArea() {
		return Math.PI*radius*radius;
	}
	
	public double circleLength() {
		return Math.PI*radius*2.0;
	}
}
