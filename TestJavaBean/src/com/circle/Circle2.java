package com.circle;

public class Circle2 {
	double radius=1;
	double circleLength=0;
	double circleArea=0;
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public double getCircleLength() {
		circleLength=Math.PI*radius*2.0;
		return circleLength;
	}
	public double getCircleArea() {
		circleArea=Math.PI*radius*radius;
		return circleArea;
	}
	
}
