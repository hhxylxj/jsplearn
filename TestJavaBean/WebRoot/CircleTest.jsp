<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ page import="com.circle.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>My JSP 'CircleTest.jsp' starting page</title>
  </head>
  <body>
  	<jsp:useBean id="boy" class="com.circle.Circle" scope="page"></jsp:useBean>
  	<%
  		boy.setRadius(10);
  	%>
  	圆的半径：<%=boy.getRadius() %><br>
  	圆的周长：<%=boy.circleLength() %><br>
  	圆的面积：<%=boy.circleArea() %>
  </body>
</html>
