<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.computer.*" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:useBean id="computer" class="com.computer.ComputerBean" scope="session" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>    
    <title>My JSP 'computer.jsp' starting page</title>
  </head>
  <body>
  	<jsp:setProperty property="*" name="computer"/>
  	<form action="" method="post" name="form">
		<input type="text" name="numberOne" value='<jsp:getProperty name="computer" property="numberOne" />' size="5" />
		<select name="operator">
			<Option value="+">+
			<Option value="-">-
			<Option value="*">*
			<Option value="/">/
		</select> 
		<input type="text" name="numberTwo" value='<jsp:getProperty name="computer" property="numberTwo" />' size="5" />
		=<jsp:getProperty property="result" name="computer"/>
		<br>
		<input type="submit" value="提交你的选择" name="submit" />
  	</form>
  </body>
</html>
