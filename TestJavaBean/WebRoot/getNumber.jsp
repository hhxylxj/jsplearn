<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.number.*"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<jsp:useBean id="guess" class="com.number.GuessNumber" scope="session"></jsp:useBean>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>My JSP 'getNumber.jsp' starting page</title>
</head>
<body>
	<%
		int n = (int) (Math.random() * 100) + 1;
		String str = response.encodeRedirectURL("guess.jsp");
	%>
	<jsp:setProperty property="answer" name="guess" value="<%=n%>" />
	随你给你一个1到100之间的数，请猜猜这个数是多少？
	<form action="<%=str%>" method="post">
		输入你的猜测：<input type="text" name="guessNumber" /> 
		<input type="submit" value="提交">
	</form>
</body>
</html>
