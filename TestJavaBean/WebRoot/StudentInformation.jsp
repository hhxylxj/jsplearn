<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@page import="com.student.*"%>
<jsp:useBean id="lisi" class="com.student.Student" scope="page"></jsp:useBean>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>My JSP 'StudentInformation.jsp' starting page</title>
</head>
<body>
	<jsp:setProperty property="name" name="lisi" value="李四" />
	名字是：<jsp:getProperty property="name" name="lisi" /><br>
	<jsp:setProperty property="number" name="lisi" value="199901" />
	学号是：<jsp:getProperty property="number" name="lisi" /><br>
	<jsp:setProperty property="height" name="lisi" value="1.78" />
	身高是：<jsp:getProperty property="height" name="lisi" />米
	<br>
	<jsp:setProperty property="weight" name="lisi" value="67.75" />
	体重是：<jsp:getProperty property="weight" name="lisi" />公斤
</body>
</html>
