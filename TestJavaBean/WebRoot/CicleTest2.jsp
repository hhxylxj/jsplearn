<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@page import="com.circle.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head> 
    <title>My JSP 'CicleTest2.jsp' starting page</title>
  </head>
  <body>
	<jsp:useBean id="opt" class="com.circle.Circle2" scope="page"></jsp:useBean>
	圆的半径：<jsp:getProperty property="radius" name="opt"/><br>
  	圆的周长：<jsp:getProperty property="circleLength" name="opt"/><br>
  	圆的面积：<jsp:getProperty property="circleArea" name="opt"/>
  </body>
</html>
