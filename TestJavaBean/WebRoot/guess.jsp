<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.number.*" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:useBean id="guess" class="com.number.GuessNumber" scope="session"></jsp:useBean>
  	<%
  		String strGuess = response.encodeRedirectURL("guess.jsp");
  		String strGetNumber = response.encodeRedirectURL("getNumber.jsp");
  	%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>My JSP 'guess.jsp' starting page</title>
  </head>
  <body>
	<jsp:setProperty property="guessNumber" name="guess" param="guessNumber"/>
	这是第<jsp:getProperty property="guessCount" name="guess" />猜，
	<jsp:getProperty property="result" name="guess"/>
	你给出的数是：<jsp:getProperty property="guessNumber" name="guess"/>
	<%if(guess.isRight()==false) {%>
	<form action="<%=strGuess %>" method="post">
		再输入你的猜想：<input type="text" name="guessNumber" />
		<input type="submit" value="提交" />
	</form>
	<%} %>
	<br>
	<a href="<%=strGetNumber %>">链接到getNumber.jsp重新玩猜数字。</a>
  </body>
</html>
