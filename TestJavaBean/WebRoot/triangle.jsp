<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.triangle.*" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:useBean id="triangle" class="com.triangle.Triangle" scope="page" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head> 
    <title>My JSP 'triangle.jsp' starting page</title>
  </head>
  
  <body>
  	<form method="post">
    	<p>输入三角形的边A：<input type="text" name="sideA" value="" /></p>
    	<p>输入三角形的边B：<input type="text" name="sideB" value="" /></p>
    	<p>输入三角形的边C：<input type="text" name="sideC" value="" /></p>
    	<input type="submit" value="提交" />
    	<p>你给出的三角形的三边是：</p>
    	<jsp:setProperty property="*" name="triangle"/>
    	<br>边A是:<jsp:getProperty property="sideA" name="triangle"/>
    	<br>边B是:<jsp:getProperty property="sideB" name="triangle"/>
    	<br>边C是:<jsp:getProperty property="sideC" name="triangle"/>
    	<p>这三个边能构成一个三角形吗？</p>
    	<jsp:getProperty property="triangle" name="triangle"/>
    	<p>面积是：<jsp:getProperty property="area" name="triangle"/></p>
    </form>
  </body>
</html>
