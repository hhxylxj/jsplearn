package com.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.mail.internet.NewsAddress;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ShowParametersMap extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		request.setCharacterEncoding("utf-8");
		String title = "Reading All Request Parameters";
		out.println("<html><head><title>读取所有参数</title></head>"
				+ "<body bgcolor='#fdf5e6'>\n" + "<h1 align=\'center\'>"
				+ title + "</h1>\n" + "<table border=\'1\' align=\'center\'>\n"
				+ "<tr bgcolor=\'#ffad00\'>\n"
				+ "<th>Parameter Name<th>Parameter Value(s)");
		Map<String, String[]> paramMap = request.getParameterMap();
		Set<Map.Entry<String, String[]>> entries = paramMap.entrySet();
		for (Iterator<Entry<String, String[]>> it = entries.iterator(); it
				.hasNext();) {
			Map.Entry<String, String[]> entry = it.next();
			String paramName = entry.getKey();
			out.print("<tr><td>" + paramName + "\n<td>");
			String[] paramValues = entry.getValue();
			if (paramValues.length == 1) {
				String paramvalue = paramValues[0];
				if (paramvalue.length() == 0)
					out.println("<i>No Value</i>");
				else {
					out.println(new String(paramvalue));
				}
			} else {
				out.println("<ul>");
				for (int i = 0; i < paramValues.length; i++) {
					out.println("<li>" + new String(paramValues[i]));
				}
				out.println("</ul>");
			}
		}
		out.println("</table>\n</body></html>");
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
